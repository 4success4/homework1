
package Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Natavan Mammadova
 */
public class Cookies extends HttpServlet {

    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession(false);
        try {
         
        out.println("<meta http-equiv=\"refresh\" content=\"0300; URL=index.html\">");
              
        String isAuthorised = (String)session.getAttribute("authorised");
        String logout = request.getParameter("logout");
        
        if (isAuthorised!=null && isAuthorised.equals("yes"))
        {
           RequestDispatcher rd = request.getRequestDispatcher("main page.jsp");
           rd.include(request, response);
        }
        else
        {
           RequestDispatcher rd = request.getRequestDispatcher("index.html");
           rd.include(request, response);
        }
          
          Cookie[] cookies = request.getCookies();
          
          if(cookies != null){
              
          for(int i = 0; i < cookies.length; i++){
              out.println("<script>" + "document.getElementById(\'inner\').innerHTML =\""
               + "Welcome "+ cookies[i].getName() + " !"+ "\"" + "</script>");
            }
          }
          else{
              
              out.println("<script>" + "document.getElementById(\'inner\').innerHTML =\""
               + "Welcome !"+ "\"" + "</script>");
              }

          if(logout!=null && logout != ""){
              
            session.invalidate();
            response.sendRedirect("index.html");
          }
              
        } finally {
            out.close();
        }
    }

   
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
